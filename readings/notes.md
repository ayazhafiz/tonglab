> 1/19/18

Can't describe an XOR function linearly

* A is +, B is +, but both negative? Not working.

Complex cel l has low cost for firing, based off the selective functions of
simple cells

* Could provide solution to selectivity invariance

> 1/23/18

### Why Tensorflow?

* Very fast
* Automatic differentiation (don't need to hard-code gradient function)

##### Learn

* Get Started + Programmer's Guide
* Look at low + high level API
* First 2-3 tutorials
* Helpful: _Jupyter Notebook_
* **LAST**: Ask Hojin for project to work on

##### Model notes

* Usually run epochs until convergence
