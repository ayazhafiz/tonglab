{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## MNIST - the easy way\n",
    "\n",
    "> Programming has `Hello World!`. ML has MNIST.\n",
    "\n",
    "MNIST is a computer vision dataset, consisting of handwritten digits and labels for which digit is which.\n",
    "\n",
    "Our goal in this notebook is to train a model to look at images and predict what digits they are. It's not going to be 100% accurate, but it'll give us a good intro into TF. We're going to use a simple model, called __Softmax Regression__.\n",
    "\n",
    "But first, let's formally list our targets:\n",
    "\n",
    "- Learn about MNIST data + softmax regressions\n",
    "- Create a function that describes a model for recognizing digits, by looking at every pixel in an image\n",
    "- Use TF to train the model to recognize digits by processing thousands of examples\n",
    "- Check the model's accuracy with test data\n",
    "\n",
    "### The MNIST Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Extracting MNIST_data/train-images-idx3-ubyte.gz\n",
      "Extracting MNIST_data/train-labels-idx1-ubyte.gz\n",
      "Extracting MNIST_data/t10k-images-idx3-ubyte.gz\n",
      "Extracting MNIST_data/t10k-labels-idx1-ubyte.gz\n"
     ]
    }
   ],
   "source": [
    "from tensorflow.examples.tutorials.mnist import input_data\n",
    "mnist = input_data.read_data_sets(\"MNIST_data/\", one_hot=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The MNIST data is split into:\n",
    "\n",
    "1. `mnist.train` - 55,000 points of training data\n",
    "2. `mnist.test` - 10,000 points of test data\n",
    "3. `mnist.validation` - 5,000 points of validation data\n",
    "\n",
    "> Note: It's important for test and validation data to be seperate from training data, so that you can ensure that what the model learns actually generalizes.\n",
    "\n",
    "Each data point consists of an image (`x`) and a label (`y`). Each image is 28x28 pixels, which can be described as a 28x28 array of numbers representing color intensity. This array can be flattened into a 784-dimensional vector. This is difficult to visualize, but it's just a dimensionally-very-rich structure.\n",
    "\n",
    "The result of this flattening is a tensor of shape [55000, 784] - there are 55,000 images, each being a vector array of 784 elements. Each element of the vector is an intensity between 0 and 1 that describes the  corresponding pixel in the image. Each image also has a corresponding label, between `0` and `9`, representing the digit drawn.\n",
    "\n",
    "There's an obvious problem with flattening the array - __it throws away information about the 2D structure__ of the image. A better model will use this (we'll see this [later on](./4-deep-mnist.ipynb)), but our softmax regression won't.\n",
    "\n",
    "Our labels are `one-hot` vectors. Such vectors are `0` in most dimensions and `1` in a single dimension. Here, a digit `n` is represented as a vector with a `1` only in the `n`th dimension. For example,\n",
    "\n",
    "```\n",
    "3 = [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]\n",
    "```'\n",
    "\n",
    "Thus, `mnist.train.labels` is a tensor of shape [55000, 10] - 55,000 labels, with each label being a 10-dimensional array."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Softmax Regressions\n",
    "\n",
    "Because there are only ten possibilities for a digit, we want out model to be able to look at an image and give the probability of it being each digit. Just like a teacher may not know if a student's `8` is actually a `9`, our model may not be 100% sure about every image it sees.\n",
    "\n",
    "Softmax regressions are the thing to do for any model where you want to assign probabilities for an object being one of several things. Softmax gives a list of values between 0 and 1 for each classification category that, in all, add up to 1. There are two steps to this:\n",
    "\n",
    "1. Add up the evidence of the input being in certain classes\n",
    "2. Convert that evidence to probabilities\n",
    "\n",
    "To tally up the evidence that an image is an a particular class, we employ a __weighted sum of pixel intensities__. The weight is positive is the high-intensity pixel is evidence for the image in that class, and negative if it is evidence against.\n",
    "\n",
    "We also include another piece of evidence, called a __bias__. Basically, we say that some things are more likely independent of the input. The result of this declaration is that the evidence for a class `i` given an input `x` is\n",
    "\n",
    "$$\\text{evidence}_i = \\sum_j W_{i,\\ j}x_j + b_i$$\n",
    "\n",
    "Where $W_i$ is the weights, $b_i$ is the bias for class $i$, and $j$ is an index for summing over the pixels in the input image $x$. \n",
    "\n",
    "This evidence can then be converted into the predicted probabilities `y` using the _softmax_ function:\n",
    "\n",
    "$$ y = \\text{softmax}(\\text{evidence})$$\n",
    "\n",
    "Softmax serves as an _activation_ or _link_ function, transforming the linear function into a probability distribution - the form we want! That's basically a convoluted way of saying we're converting tallies into probabilities. We can define this as\n",
    "\n",
    "$$ \\text{softmax}(evidence) = \\text{normalize}(\\text{exp}(evidence))$$\n",
    "\n",
    "Or expanded as\n",
    "\n",
    "$$ \\text{softmax}(evidence)_i = \\frac{\\text{exp}(evidence_i)}{\\sum_j \\text{exp}(evidence_j)}$$\n",
    "\n",
    "A helpful way to think of softmax is as exponentiating its inputs, and then normalizing them. The exponentiation means that the addition of one unit of evidence increases the total weight given to any hypothesis __multiplicatively__. This gives a substantial weight to the highest-tallied hypothesis, and lower-tallied hypothesis a small probability. These exponentially-weighted tallies are then normalized so the sum of the weights is one, creating a valid probability distribution.\n",
    "\n",
    "To summarize, probabilities of each class are computed the following way:\n",
    "\n",
    "1. Tally up weighted weighted sum of pixel intensities\n",
    "    - weight is positive if it supports class, negative otherwise\n",
    "2. Add _bias_ to tallies\n",
    "3. `softmax`: Exponentiate total number of tallies and then normalize them\n",
    "\n",
    "<img style=\"width: 80%\" alt=\"Softmax visualization\" src=\"https://www.tensorflow.org/images/softmax-regression-scalargraph.png\">\n",
    "\n",
    "Or, as Gili Golan will like,\n",
    "\n",
    "<img style=\"width: 80%\" alt=\"Softmax visualization\" src=\"https://www.tensorflow.org/images/softmax-regression-vectorequation.png\">\n",
    "\n",
    "Or,\n",
    "\n",
    "$$y = \\text{softmax}(Wx + b)$$\n",
    "\n",
    "This is cool, but now let's make it into something that TensorFlow can actually use."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Implementing the Softmax Regression\n",
    "\n",
    "TF's heavy lifting is done in C++, but it is expensive to switch back and forth between the languages for many operations. To reduce this cost, we can describe a graph of interaction operations to run all at once.\n",
    "\n",
    "Okay, let's do the implementation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "\n",
    "x = tf.placeholder(tf.float32, [None, 784])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`x` is a lamda-like `placeholder` that will take an input when the computation is run, because we want to be able to input any member of the MNIST images, flattened as a 784-dimensional vector. We use `None` because it is a 2D tensor, and we want the dimension (number of total images, in this specific example 55,000) to be any variable number.\n",
    "\n",
    "We also need weights and biases for the model. TF has a convinient way to handle this - `Variable`s. A `Variable` is a modifiable tensor living in TF's graph of interacting operations, avaliable for use and modification by the computation. In ML, these usually define the model parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "W = tf.Variable(tf.zeros([784, 10])) # weights\n",
    "b = tf.Variable(tf.zeros([10])) # biases"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can initialize both the weight and bias variables as tensors full of zeros, since we're going to learn what they are anyway.\n",
    "\n",
    "`W` is a tensor of shape [784, 10] because we will multiply the 784-dimensional image vectors by it to get 10-dimensional vectors of evidence. Of course, `b` is of shape [10] so we can add it to the output of that multiplication.\n",
    "\n",
    "Now we can implement our model! We want to matrix multiply `x` and `W`, in that order, to deal with `x` being a 2D vector with multiple inputs. Then we add the bias `b` and apply `softmax` regression. Damn, TensorFlow makes softmax easy! The reason for this is that softmax is a very flexible way to describe many numerical computations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = tf.nn.softmax(tf.matmul(x, W) + b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Training\n",
    "\n",
    "To train our model, we must first define what it means for the model to be _good_ - or, in ML, what it means for a model to be _bad_. This is done via a __cost__, or __loss__ function, which represents how far our model is from the expected (and desired outcome). The smaller the loss, the better our model.\n",
    "\n",
    "A nice loss function ito use is __cross-entropy__, which arises from thinking about information-compressing codes in information theory, but that's not important. It's defined as\n",
    "\n",
    "$$H_{y'}(y) = -\\sum_iy'\\log(y_i)$$\n",
    "\n",
    "where $y$ is the predicted probability distribution and $y'$ is the true distribution of one-hot digit label vectors. Roughly, the corss-entropy measures how inefficient the prediction is for describing the truth.\n",
    "\n",
    "To implement cross-entropy we have to first create a placeholder to input the correct answers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "_y = tf.placeholder(tf.float32, [None, 10])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we can implement the cross-entropy function $-\\sum y'\\log(y)$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "cross_entropy = tf.reduce_mean(\n",
    "    -tf.reduce_sum(_y * tf.log(y), reduction_indices=[1]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`reduction_indices=[1]` adds the elements in the second dimension of `y` (which we expect to be the evaluated \"number\" as a 10-dimensional vector), and `tf.reduce_mean` computes the mean over all the samples in the batch.\n",
    "\n",
    "In actual code, `tf.nn.softmax_cross_entropy_with_logits` should be called instead of creating `cross_entropy`, because the predefined function is more numerically stable and internally computes the softmax activation.\n",
    "\n",
    "Okay, cool! We know what we want our model to do, so it's very easy to have TensorFlow train it to do so. TF knows the entire computational graph, so it can automatically use the [backpropagation algorithm](http://colah.github.io/posts/2015-08-Backprop/) to determine, efficiently, how the variables affect the loss we're trying to minimize. \n",
    "\n",
    "It can then apply any choice of optimization algorithm to modify the variables and reduce the loss."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, we're using TF to minimize `cross_entropy` using gradient descent with a learning rate of `0.5`.\n",
    "\n",
    "##### But what does TensorFlow actually do?\n",
    "\n",
    "Behind the scenes, TF adds new operations to the graph, which implement backpropagation and gradient descent. Afterward, it returns a single operation which, when run, does a step of gradient descent training, tweaking the variables to reduce the loss.\n",
    "\n",
    "We'll launch the model in an `InteractiveSession` and initialize the variables we previously made."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [],
   "source": [
    "sess = tf.InteractiveSession()\n",
    "tf.global_variables_initializer().run()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we can train the model! For each iteration of 1000, we run a `train_step` on a stochastic, 100 random data points from the training set. Stochastic gradient descent in action!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [],
   "source": [
    "for _ in range(1000):\n",
    "    batch_x, batch_y = mnist.train.next_batch(100)\n",
    "    sess.run(train_step, feed_dict={x: batch_x, _y: batch_y})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluating the Model\n",
    "\n",
    "First, we need to figure out where we predicted the correct label. `tf.argmax` can be used for this, as it gives the index of the highest entry in a tensor along some axis. For example, `tf.argmax(y, 1)` is the label our model thinks is most likely for each input, and the correct label is `tf.argmax(_y, 1)`. We can then simply compare the two, giving us an array of booleans. Cast this to ints, take the mean of the array, and you get the accuracy!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.92\n"
     ]
    }
   ],
   "source": [
    "correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(_y, 1))\n",
    "accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))\n",
    "print(sess.run(accuracy, feed_dict={x: mnist.test.images, _y: mnist.test.labels}))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That's okay, but we can do a lot better. Let's [see how](./4-mnist-deep.ipynb)!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
