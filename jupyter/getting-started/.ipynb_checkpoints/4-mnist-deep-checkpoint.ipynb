{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deeper MNIST\n",
    "\n",
    "This time, we're going to make a non-trivial version of the MNIST Softmax model, improving its accuracy. Our goals for this notebook are:\n",
    "\n",
    "- Create a softmax regression function for recognizing MNIST digits,\n",
    "- Use TF to train the model,\n",
    "- Check the model's accuracy with test data,\n",
    "- Build, train, and test a multilayer CNN to improve the results."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Starting off\n",
    "\n",
    "Let's start off where we left off last time. If any of the code doesn't make sense to you, reference the [basic MNIST](./3-mnist.ipynb) for the logic."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Extracting MNIST_data/train-images-idx3-ubyte.gz\n",
      "Extracting MNIST_data/train-labels-idx1-ubyte.gz\n",
      "Extracting MNIST_data/t10k-images-idx3-ubyte.gz\n",
      "Extracting MNIST_data/t10k-labels-idx1-ubyte.gz\n",
      "\n",
      "Accuracy: 0.920\n"
     ]
    }
   ],
   "source": [
    "import tensorflow as tf\n",
    "from tensorflow.examples.tutorials.mnist import input_data\n",
    "mnist = input_data.read_data_sets('MNIST_data', one_hot=True)\n",
    "\n",
    "sess = tf.InteractiveSession()\n",
    "\n",
    "# Placeholders\n",
    "x = tf.placeholder(tf.float32, shape=[None, 784])\n",
    "_y = tf.placeholder(tf.float32, shape=[None, 10])\n",
    "\n",
    "# Variables\n",
    "W = tf.Variable(tf.zeros([784, 10]))\n",
    "b = tf.Variable(tf.zeros([10]))\n",
    "sess.run(tf.global_variables_initializer())\n",
    "\n",
    "# Predicted Class and Loss Function\n",
    "y = tf.matmul(x, W) + b\n",
    "cross_entropy = tf.reduce_mean( # take average of all prediction sums\n",
    "    tf.nn.softmax_cross_entropy_with_logits(labels=_y, logits=y))\n",
    "\n",
    "# Train the model\n",
    "train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)\n",
    "\n",
    "for _ in range(1000):\n",
    "    batch = mnist.train.next_batch(100)\n",
    "    train_step.run(feed_dict={x: batch[0], _y: batch[1]})\n",
    "    \n",
    "# Evaluate the Model\n",
    "correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(_y, 1))\n",
    "accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))\n",
    "print('\\nAccuracy: %.3f' % accuracy.eval(\n",
    "    feed_dict={x: mnist.test.images, _y: mnist.test.labels}))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, for the big leagues.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Building a Multilayer Convoluntional Network\n",
    "\n",
    "We can do better that 92%. In fact, the convolutional neural network we'll soon build will get us to ~99.2%. Here's a simple representation of what we're going for:\n",
    "\n",
    "<img style=\"width:40%\" alt=\"CNN model\" src=\"https://www.tensorflow.org/images/mnist_deep.png\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Weight Initialization\n",
    "\n",
    "This is a moderately sophisticated model, and we're going to need a lot of weights and biases to make it work. Generally, we want to create initialize weights with a _small amount of noise_ in order to __break symmetry__ and __prevent 0 gradients__.\n",
    "\n",
    "We're using ReLU neurons, so it's a good idea to initialize them with a slightly positive initial bias in order to avoid _dead neurons_ that will never reach the ReLU cutoff."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [],
   "source": [
    "def weight_variable(shape):\n",
    "    initial = tf.truncated_normal(shape, stddev=0.1) # noise\n",
    "    return tf.Variable(initial)\n",
    "\n",
    "def bias_variable(shape):\n",
    "    initial = tf.constant(0.1, shape=shape) # lightly positive bias\n",
    "    return tf.Variable(initial)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Convolution and Pooling\n",
    "\n",
    "TF gives the user a lot of flexibility in convolution and pooling operations. In this example, we're going to stay basic - our convolutions have a stride of `1` and and 0-padded so that the output matches the size of the input, and the pooling is max pooling over `2x2` blocks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [],
   "source": [
    "def conv2d(x, W):\n",
    "    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')\n",
    "\n",
    "def max_pool_2x2(x):\n",
    "    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],\n",
    "                          strides=[1, 2, 2, 1], padding='SAME')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### First Convolutional Layer\n",
    "\n",
    "The first layer consists of convolution, followed by max pooling. This convolution will compute 32 features for each 5x5 patch in the image, so its weight tensor will have a shape of [5, 5, 1, 32], corresponding to the patch size (5x5), the number of input channels (one image), and the number of output channels (32 features of 5x5 patches). We'll also include a bias vector with a component for each output channel."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [],
   "source": [
    "W_conv1 = weight_variable([5, 5, 1, 32])\n",
    "b_conv1 = bias_variable([32])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To apply the first layer, we have to reshape `x` to a 4d tensor, with the second and third dimensions representing the image width and height, and the last dimension being the number of color channels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {},
   "outputs": [],
   "source": [
    "x_image = tf.reshape(x, [-1, 28, 28, 1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, convolve `x_image` with the weight tensor, add bias, apply ReLU, and max pool - the `max_pool_2x2` method we defined before will reduce our image size to 14x14."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {},
   "outputs": [],
   "source": [
    "h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)\n",
    "h_pool1 = max_pool_2x2(h_conv1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Second Convolutional Layer\n",
    "\n",
    "A deep network involves stacking several layers of the first type; the second layer will include 64 features for each 5x5 patch."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {},
   "outputs": [],
   "source": [
    "W_conv2 = weight_variable([5, 5, 32, 64]) # takes 32 inputs from first layer\n",
    "b_conv2 = bias_variable([64])\n",
    "\n",
    "h_conv2 = tf.nn.relu(\n",
    "    conv2d(h_pool1, W_conv2) + b_conv2) # take pool from first layer\n",
    "h_pool2 = max_pool_2x2(h_conv2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Densely Connected Layer\n",
    "\n",
    "The image size is now reduced to 7x7, and so we can add a fully-connected layer with 1024 neurons to allow full-image processing. We want to reshape the tensor from the pooling layer to a batch of vectors, multiply that by a weight matrix, apply a bias, and apply a ReLU to that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "metadata": {},
   "outputs": [],
   "source": [
    "W_fc1 = weight_variable([7 * 7 * 64, 1024]) # 64 inputs, by each pixel\n",
    "b_fc1 = bias_variable([1024])\n",
    "\n",
    "h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])\n",
    "h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dropout\n",
    "\n",
    "We apply dropout before the readout layer to reduce overfitting (this reduced interdependence between neurons). We'll create a `placeholder` for the probability of a neuron's output being kept during dropout. This allows us to turn dropout on and off during training and testing, respectively. TF's `tf.nn.dropout` automatically handles scaling neuron outputs, in addition to masking them, which means that dropout works without any additional scaling.\n",
    "\n",
    "> Note that in this particular case, dropout doesn't improve performance __too much__. Dropout is very effective at reducing overfitting, but mostly only on __very large__ neural networks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "metadata": {},
   "outputs": [],
   "source": [
    "keep_prob = tf.placeholder(tf.float32)\n",
    "h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Readout Layer\n",
    "\n",
    "Lastly, we add a layer to readout the model, similar to the softmax regression we did before."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "metadata": {},
   "outputs": [],
   "source": [
    "W_fc2 = weight_variable([1024, 10])\n",
    "b_fc2 = bias_variable([10])\n",
    "\n",
    "y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Train and Evaluate the Model\n",
    "\n",
    "Training and evaluation uses _almost_ exactly the same code as the softmax network used, the main differences being\n",
    "\n",
    "- We'll replace the steepest gradient descent optimizer with a more sophisticated __ADAM optimizer__.\n",
    "- We'll include the parameter `keep_prob` in `feed_dict` to control the dropout rate.\n",
    "- We'll log every 100th iteration in the training process.\n",
    "- We'll use `tf.Session` rather than `tf.InteractiveSession` to better seperate the process of _creating the graph_ (model spec) and _evaluating the graph_ (model fitting), which makes cleaner code.\n",
    "    - `tf.Session` is created in a `with` block so it's automatically destroyed once the block is exited."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "step     0 - training accuracy: 0.12\n",
      "step   100 - training accuracy: 0.88\n",
      "step   200 - training accuracy: 0.88\n",
      "step   300 - training accuracy: 0.98\n",
      "step   400 - training accuracy: 0.88\n",
      "step   500 - training accuracy: 0.94\n",
      "step   600 - training accuracy: 0.88\n",
      "step   700 - training accuracy: 1\n",
      "step   800 - training accuracy: 0.96\n",
      "step   900 - training accuracy: 0.98\n",
      "step  1000 - training accuracy: 0.94\n",
      "step  1100 - training accuracy: 1\n",
      "step  1200 - training accuracy: 1\n",
      "step  1300 - training accuracy: 1\n",
      "step  1400 - training accuracy: 0.96\n",
      "step  1500 - training accuracy: 0.98\n",
      "step  1600 - training accuracy: 0.96\n",
      "step  1700 - training accuracy: 0.98\n",
      "step  1800 - training accuracy: 0.92\n",
      "step  1900 - training accuracy: 1\n",
      "step  2000 - training accuracy: 1\n",
      "step  2100 - training accuracy: 1\n",
      "step  2200 - training accuracy: 0.94\n",
      "step  2300 - training accuracy: 0.96\n",
      "step  2400 - training accuracy: 0.98\n",
      "step  2500 - training accuracy: 1\n",
      "step  2600 - training accuracy: 0.98\n",
      "step  2700 - training accuracy: 0.98\n",
      "step  2800 - training accuracy: 0.98\n",
      "step  2900 - training accuracy: 1\n",
      "step  3000 - training accuracy: 1\n",
      "step  3100 - training accuracy: 1\n",
      "step  3200 - training accuracy: 0.98\n",
      "step  3300 - training accuracy: 0.98\n",
      "step  3400 - training accuracy: 0.98\n",
      "step  3500 - training accuracy: 1\n",
      "step  3600 - training accuracy: 1\n",
      "step  3700 - training accuracy: 0.98\n",
      "step  3800 - training accuracy: 1\n",
      "step  3900 - training accuracy: 0.98\n",
      "step  4000 - training accuracy: 1\n",
      "step  4100 - training accuracy: 1\n",
      "step  4200 - training accuracy: 0.96\n",
      "step  4300 - training accuracy: 0.98\n",
      "step  4400 - training accuracy: 1\n",
      "step  4500 - training accuracy: 0.98\n",
      "step  4600 - training accuracy: 1\n",
      "step  4700 - training accuracy: 1\n",
      "step  4800 - training accuracy: 0.98\n",
      "step  4900 - training accuracy: 1\n",
      "step  5000 - training accuracy: 0.96\n",
      "step  5100 - training accuracy: 1\n",
      "step  5200 - training accuracy: 1\n",
      "step  5300 - training accuracy: 1\n",
      "step  5400 - training accuracy: 1\n",
      "step  5500 - training accuracy: 1\n",
      "step  5600 - training accuracy: 1\n",
      "step  5700 - training accuracy: 1\n",
      "step  5800 - training accuracy: 1\n",
      "step  5900 - training accuracy: 1\n",
      "step  6000 - training accuracy: 1\n",
      "step  6100 - training accuracy: 1\n",
      "step  6200 - training accuracy: 0.98\n",
      "step  6300 - training accuracy: 1\n",
      "step  6400 - training accuracy: 0.98\n",
      "step  6500 - training accuracy: 1\n",
      "step  6600 - training accuracy: 1\n",
      "step  6700 - training accuracy: 0.96\n",
      "step  6800 - training accuracy: 1\n",
      "step  6900 - training accuracy: 1\n",
      "step  7000 - training accuracy: 1\n",
      "step  7100 - training accuracy: 1\n",
      "step  7200 - training accuracy: 1\n",
      "step  7300 - training accuracy: 1\n",
      "step  7400 - training accuracy: 1\n",
      "step  7500 - training accuracy: 0.98\n",
      "step  7600 - training accuracy: 1\n",
      "step  7700 - training accuracy: 1\n",
      "step  7800 - training accuracy: 1\n",
      "step  7900 - training accuracy: 0.98\n",
      "step  8000 - training accuracy: 1\n",
      "step  8100 - training accuracy: 1\n",
      "step  8200 - training accuracy: 1\n",
      "step  8300 - training accuracy: 0.98\n",
      "step  8400 - training accuracy: 1\n",
      "step  8500 - training accuracy: 0.98\n",
      "step  8600 - training accuracy: 1\n",
      "step  8700 - training accuracy: 0.96\n",
      "step  8800 - training accuracy: 1\n",
      "step  8900 - training accuracy: 1\n",
      "step  9000 - training accuracy: 1\n",
      "step  9100 - training accuracy: 1\n",
      "step  9200 - training accuracy: 1\n",
      "step  9300 - training accuracy: 0.98\n",
      "step  9400 - training accuracy: 1\n",
      "step  9500 - training accuracy: 0.98\n",
      "step  9600 - training accuracy: 0.98\n",
      "step  9700 - training accuracy: 0.98\n",
      "step  9800 - training accuracy: 1\n",
      "step  9900 - training accuracy: 1\n",
      "step 10000 - training accuracy: 0.98\n",
      "step 10100 - training accuracy: 1\n",
      "step 10200 - training accuracy: 1\n",
      "step 10300 - training accuracy: 0.98\n",
      "step 10400 - training accuracy: 0.98\n",
      "step 10500 - training accuracy: 1\n",
      "step 10600 - training accuracy: 1\n",
      "step 10700 - training accuracy: 1\n",
      "step 10800 - training accuracy: 0.98\n",
      "step 10900 - training accuracy: 1\n",
      "step 11000 - training accuracy: 1\n",
      "step 11100 - training accuracy: 1\n",
      "step 11200 - training accuracy: 1\n",
      "step 11300 - training accuracy: 1\n",
      "step 11400 - training accuracy: 0.98\n",
      "step 11500 - training accuracy: 1\n",
      "step 11600 - training accuracy: 0.98\n",
      "step 11700 - training accuracy: 1\n",
      "step 11800 - training accuracy: 1\n",
      "step 11900 - training accuracy: 1\n",
      "step 12000 - training accuracy: 0.98\n",
      "step 12100 - training accuracy: 1\n",
      "step 12200 - training accuracy: 1\n",
      "step 12300 - training accuracy: 1\n",
      "step 12400 - training accuracy: 1\n",
      "step 12500 - training accuracy: 0.98\n",
      "step 12600 - training accuracy: 1\n",
      "step 12700 - training accuracy: 0.98\n",
      "step 12800 - training accuracy: 1\n",
      "step 12900 - training accuracy: 1\n",
      "step 13000 - training accuracy: 1\n",
      "step 13100 - training accuracy: 1\n",
      "step 13200 - training accuracy: 1\n",
      "step 13300 - training accuracy: 1\n",
      "step 13400 - training accuracy: 0.98\n",
      "step 13500 - training accuracy: 1\n",
      "step 13600 - training accuracy: 1\n",
      "step 13700 - training accuracy: 1\n",
      "step 13800 - training accuracy: 1\n",
      "step 13900 - training accuracy: 1\n",
      "step 14000 - training accuracy: 1\n",
      "step 14100 - training accuracy: 1\n",
      "step 14200 - training accuracy: 1\n",
      "step 14300 - training accuracy: 1\n",
      "step 14400 - training accuracy: 1\n",
      "step 14500 - training accuracy: 1\n",
      "step 14600 - training accuracy: 1\n",
      "step 14700 - training accuracy: 1\n",
      "step 14800 - training accuracy: 1\n",
      "step 14900 - training accuracy: 1\n",
      "step 15000 - training accuracy: 1\n",
      "step 15100 - training accuracy: 1\n",
      "step 15200 - training accuracy: 1\n",
      "step 15300 - training accuracy: 1\n",
      "step 15400 - training accuracy: 1\n",
      "step 15500 - training accuracy: 0.98\n",
      "step 15600 - training accuracy: 1\n",
      "step 15700 - training accuracy: 1\n",
      "step 15800 - training accuracy: 0.98\n",
      "step 15900 - training accuracy: 1\n",
      "step 16000 - training accuracy: 1\n",
      "step 16100 - training accuracy: 1\n",
      "step 16200 - training accuracy: 1\n",
      "step 16300 - training accuracy: 1\n",
      "step 16400 - training accuracy: 1\n",
      "step 16500 - training accuracy: 1\n",
      "step 16600 - training accuracy: 1\n",
      "step 16700 - training accuracy: 1\n",
      "step 16800 - training accuracy: 1\n",
      "step 16900 - training accuracy: 1\n",
      "step 17000 - training accuracy: 1\n",
      "step 17100 - training accuracy: 1\n",
      "step 17200 - training accuracy: 1\n",
      "step 17300 - training accuracy: 1\n",
      "step 17400 - training accuracy: 1\n",
      "step 17500 - training accuracy: 1\n",
      "step 17600 - training accuracy: 1\n",
      "step 17700 - training accuracy: 1\n",
      "step 17800 - training accuracy: 0.98\n",
      "step 17900 - training accuracy: 1\n",
      "step 18000 - training accuracy: 1\n",
      "step 18100 - training accuracy: 1\n",
      "step 18200 - training accuracy: 1\n",
      "step 18300 - training accuracy: 1\n",
      "step 18400 - training accuracy: 0.98\n",
      "step 18500 - training accuracy: 0.98\n",
      "step 18600 - training accuracy: 1\n",
      "step 18700 - training accuracy: 1\n",
      "step 18800 - training accuracy: 1\n",
      "step 18900 - training accuracy: 1\n",
      "step 19000 - training accuracy: 1\n",
      "step 19100 - training accuracy: 1\n",
      "step 19200 - training accuracy: 1\n",
      "step 19300 - training accuracy: 1\n",
      "step 19400 - training accuracy: 1\n",
      "step 19500 - training accuracy: 1\n",
      "step 19600 - training accuracy: 1\n",
      "step 19700 - training accuracy: 1\n",
      "step 19800 - training accuracy: 1\n",
      "step 19900 - training accuracy: 1\n",
      "\n",
      "test accuracy: 0.9925\n"
     ]
    }
   ],
   "source": [
    "cross_entropy = tf.reduce_mean(\n",
    "    tf.nn.softmax_cross_entropy_with_logits(labels=_y, logits=y_conv))\n",
    "train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)\n",
    "\n",
    "correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(_y, 1))\n",
    "accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))\n",
    "\n",
    "with tf.Session() as sess:\n",
    "    sess.run(tf.global_variables_initializer())\n",
    "    for i in range(20000):\n",
    "        batch = mnist.train.next_batch(50)\n",
    "        if i % 100 == 0: # log 100th iteration\n",
    "            train_accuracy = accuracy.eval(feed_dict={\n",
    "                x: batch[0], _y: batch[1], keep_prob: 1.0})\n",
    "            print('step %5d - training accuracy: %g' % (i, train_accuracy))\n",
    "        train_step.run(feed_dict={x: batch[0], _y: batch[1], keep_prob: 0.5})\n",
    "    \n",
    "    print('\\ntest accuracy: %g' % accuracy.eval(feed_dict={\n",
    "        x: mnist.test.images, _y: mnist.test.labels, keep_prob: 1.0}))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This deep learning model is more sophisticated and does much better."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
