{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Assure that TensorFlow is working"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello, TensorFlow!\n"
     ]
    }
   ],
   "source": [
    "import tensorflow as tf\n",
    "hello = tf.constant('Hello, TensorFlow!')\n",
    "sess = tf.Session()\n",
    "print(sess.run(hello))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computational Graph\n",
    "Generally, TensorFlow Core (the low-level API of TensorFlow) consists of\n",
    "1. Building a computational graph\n",
    "2. Running that graph\n",
    "\n",
    "A computational graph is defined to be a graph of nodes of TF operations. Each __node__ in this graph takes __0+ tensors as inputs__ and __1 tensor as an output__.\n",
    "\n",
    "A trivial type of node is a __constant__. It takes no inputs, and outputs an internally-held value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(<tf.Tensor 'Const_7:0' shape=() dtype=float32>, <tf.Tensor 'Const_8:0' shape=() dtype=float32>)\n"
     ]
    }
   ],
   "source": [
    "node1 = tf.constant(3.0, dtype=tf.float32)\n",
    "node2 = tf.constant(4.0) # implicitly of type `tf.float32`\n",
    "print(node1, node2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Why do the nodes not print out their values?\n",
    "\n",
    "Nodes must be _evaluated_ to produce their internally-held values. To evaluate them, the computational graph must be run within a __session__. Sessions encapsulate the _control and state_ of the TF runtime.\n",
    "\n",
    "We can use the `#run` method to run a part of the computational graph - in this case, to evaluate `node1` and `node2`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[3.0, 4.0]\n"
     ]
    }
   ],
   "source": [
    "sess = tf.Session()\n",
    "print(sess.run([node1, node2]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Operations__ are also nodes, and allow for the building of more compilcated computations. For instance, we can define a node to add our existing two nodes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "adder_node: Tensor(\"Add_1:0\", shape=(), dtype=float32)\n",
      "sess.run(adder_node): 7.0\n"
     ]
    }
   ],
   "source": [
    "from __future__ import print_function\n",
    "adder_node = tf.add(node1, node2)\n",
    "print(\"adder_node:\", adder_node)\n",
    "print(\"sess.run(adder_node):\", sess.run(adder_node))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, this graph is not particularly useful because it will always produce the same, constant result. We make it much more interesting by parameterizing it to accept external inputs, called __placeholders__. Placeholders are essentially promises to provide a value at a later time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "7.5\n",
      "[ 3.  7.]\n"
     ]
    }
   ],
   "source": [
    "a = tf.placeholder(tf.float32)\n",
    "b = tf.placeholder(tf.float32)\n",
    "adder_node = a + b # same as `tf.add(a, b)`\n",
    "\n",
    "print(sess.run(adder_node, {a: 3, b: 4.5}))\n",
    "print(sess.run(adder_node, {a: [1, 3], b: [2, 4]}))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The placeholders allow for the creation of a lambda-like construct which we can evaluate at any time, and even with multiple inputs! We can extend it even more with a mutliplier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "22.5\n"
     ]
    }
   ],
   "source": [
    "mult_by_three = adder_node * 3.\n",
    "print(sess.run(mult_by_three, {a: 3, b: 4.5}))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, `mult_by_three` \"inherets\" the placeholders of `adder_node`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Variables\n",
    "To make any model trainable, it must be able to generate new outputs with the same input. __Variables__ allow for the addition of _trainable parameters_ to a graph. They are constructed with an initial value and a type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "M = tf.Variable([.3], dtype=tf.float32)\n",
    "b = tf.Variable([-.3], dtype=tf.float32)\n",
    "x = tf.placeholder(tf.float32)\n",
    "linear_model = M * x + b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that _constructing_ a variable __does not__ _initialize_ it, unlike `tf.constant`s, which are immediately created and cannot be changed. Variables must be initialized by calling a special operation, which creates a __handle__ to the TF sub-graph that initializes all the global variables. So, the variables are uninitialized until the handle is run with `#sess.run`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [],
   "source": [
    "init = tf.global_variables_initializer()\n",
    "sess.run(init)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`linear_model` can be evaluated for several values of `x` simultaneously."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 0.          0.30000001  0.60000002  0.90000004]\n"
     ]
    }
   ],
   "source": [
    "print(sess.run(linear_model, {x: [1, 2, 3, 4]}))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have a model. Great! Unfortunately, it's completely useless unless we know how good it is - and for that, we need some placeholder `y` to provide the desired values, and we need to define some loss function.\n",
    "\n",
    "A _loss function_ is important because it measures the distance from the current model to the expected data. A standard loss model here would be to use the sum of the square of each delta between the model and the expected data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "23.66\n"
     ]
    }
   ],
   "source": [
    "y = tf.placeholder(tf.float32)\n",
    "squared_deltas = tf.square(linear_model - y) # variables and placeholders are \"nested\"\n",
    "loss = tf.reduce_sum(squared_deltas)\n",
    "print(sess.run(loss, {x: [1, 2, 3, 4], y: [0, -1, -2, -3]}))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So how do we reduce the loss? An obvious solution is to manually adjust `M` and `b`, but that's not the goal of TensorFlow. Instead, we need the power of... ___MACHINE LEARNING___."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `tf.train` API\n",
    "\n",
    "In this trivial example, TF trains the linear model by providing __optimizers__ that slowly change each variable, getting closer and closer to minimizing the loss function. The simplest of these optimizers is a classic function called __gradient descent__, which modifies each varaible according to the magnitude of the _derivative of loss_ with respect to that variable. The great thing about TensorFlow is that it computes these gradient derivatives _for you_, meaning that you only have to provide the model and loss function!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "M: [-0.9999969] b: [ 0.99999082] loss: 5.69997e-11\n"
     ]
    }
   ],
   "source": [
    "optimizer = tf.train.GradientDescentOptimizer(0.01) # 0.01 = default learning rate\n",
    "train = optimizer.minimize(loss)\n",
    "sess.run(init) # reset model to default, incorrect values\n",
    "x_train = [1, 2, 3, 4]\n",
    "y_train = [0, -1, -2, -3]\n",
    "for i in range(1000): # 1000 trials\n",
    "    sess.run(train, {x: x_train, y: y_train})\n",
    "\n",
    "res_M, res_b, res_loss = sess.run([M, b, loss], {x: x_train, y: y_train})\n",
    "print(\"M: %s b: %s loss: %s\"%(res_M, res_b, res_loss))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Near-zero loss - awesome! Go ML + TF!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
